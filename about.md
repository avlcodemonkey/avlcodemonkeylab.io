---
layout: page
title: About
---

I am a web developer from Asheville, NC.  I've been working in the field for 15+ years, and coding in a dark corner at home for a lot longer.

I'm going to use this site mostly as a landing page for my Github project pages.  Hopefully, you'll find something useful.