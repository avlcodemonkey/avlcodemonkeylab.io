---
layout: post
title: Making The Leap
comments: false
tags: [ miscellaneous,  ]
---

I've used GitHub and BitBucket for various projects over the years.  I'm looking at making the leap to GitLab now though. 

The process of moving repos/projects over is about as easy as it gets. It did take me some time to get Jekyll to run on GitLab though. I've got the build process working, and got my path issues fixed. Now I just have to figure out how CNAMEs work with GitLab and I'll be in business.