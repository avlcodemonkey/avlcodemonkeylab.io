---
layout: post
title: Opening Up
comments: true
tags: [ ]
---

I've been spending my free time for the last year working on a private repo - creating a user friendly reporting engine for SQL Server.  It's a single page application using [Alertify](http://alertifyjs.com/) and [Mithril](https://github.com/lhorie/mithril.js), and no jQuery. I've written a ton of javascript for that project. I'm not ready to release it into the wild yet. But, I've created a few interesting libraries as part of that project. And made some interesting modifications to existing libraries. I'm going to start releasing those here. Maybe someone else can get some use out of them.