---
layout: post
title: PagedTable
comments: true
tags: [ javascript,  ]
---

I've set up a demo of my [PagedTable](http://avlcodemonkey.gitlab.io/PagedTable/) library. It adds searching, sorting, and paging to an HTML table. It's vanilla JS, so no JS dependencies, just Bootstrap/Font-Awesome CSS for styling. As a result, it's quite speedy.

Set up is simple. Include the JS/CSS files. Update your table TH elements to include a data-type attribute. Then initialize the table with one line of JS:

```javascript
<script type="text/javascript">
    var t = new PagedTable(document.getElementById('table'));
</script>
```

This script won't work with older browsers like IE9, but any modern browser should be supported, mobile included.