---
layout: post
title: The Site Awakens
comments: false
tags: [ miscellaneous,  ]
---

It's something like twelve hours until `The Force Awakens` opens in the US, and it's pretty much impossible not to hear about it. So that explains the title of this page.

This is my first foray into [Github Pages](http://pages.github.com), so I'm using [jekyll](http://jekyllrb.com) with the [lanyon](http://github.com/poole/lanyon) theme to get a quickstart. I don't intend this site to have an abundance of content; it's really just a landing page to aggregate the documentation for projects I intend to start sharing soon.