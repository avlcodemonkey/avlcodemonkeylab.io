### avlcodemonkey.gitlab.io
View the site [here](http://avlcodemonkey.gitlab.io). 

### Layout by Lanyon
Thanks to [Mark Otto](https://github.com/mdo) for a great theme

### Resources
* [Joshua Lande's Setup Guide](http://joshualande.com/jekyll-github-pages-poole/)
* [Patrick Steadman's Setup Guide](http://patricksteadman.ca/2014/08/04/lanyonsetup/)
* [Kyle Stratis' Setup Guide](http://kylestratis.com/2015/04/17/blog-setup/)
*

### License
MIT License
